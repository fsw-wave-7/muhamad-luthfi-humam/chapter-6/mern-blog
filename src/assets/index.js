import RegisterBg from './images/register-bg.jpg';
import LoginBg from './images/login-bg.jpg';
import LogoLuthfi from './images/logo-luthfi.png'

// icon
import ICGithub from './icon/github.svg'
import ICInstagram from './icon/instagram.svg'
import ICTelegram from './icon/telegram.svg'

export {RegisterBg, LoginBg, LogoLuthfi, ICGithub, ICInstagram, ICTelegram};