import React from 'react'
import { RegisterBg } from '../../../assets'
import { Button, Gap } from '../../atoms'
import './blogItem.scss'
import {useHistory} from 'react-router-dom'

const BlogItem = () => {
    const history = useHistory()
    return (
        <div className='blog-item'>
            <img className='image-thumb' src={RegisterBg} alt='post' />
            <div className='content-detail'>
                <p className='title'>Title Blog</p>
                <p className='author'>Author - Date Post</p>
                <p className='body'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <Gap height={20} />
                <Button title='View Detail' onClick={(useHistory) => history.push('/detail-blog') } />
            </div>
            
        </div>
    )
}

export default BlogItem
