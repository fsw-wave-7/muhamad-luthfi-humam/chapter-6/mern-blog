import React from 'react'
import { LogoLuthfi, ICGithub, ICInstagram, ICTelegram } from '../../../assets'
import './footer.scss'

const Icon = ({img}) => {
    return (
        <div className='icon-wrapper'>
            <img className='icon-medsos' src={img} alt ='icon' />
        </div>
    )
}
const Footer = () => {
    return (
        <div>
            <div className='footer'>
                <div>
                    <img className='logo' src={LogoLuthfi} alt="logo luthfi"/>
                </div>
                <div className='social-wrapper'>
                    <Icon img={ICGithub} alt="github" />
                    <Icon img={ICInstagram} alt="instagram" />
                    <Icon img={ICTelegram} alt="telegram" />
                </div> 
            </div>
            <div className='copyright'>
                <p>Copyright &copy; 2021 Luthfi Humam</p>
            </div>
            
        </div>
    )
}

export default Footer
